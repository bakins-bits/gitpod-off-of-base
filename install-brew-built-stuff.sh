sudo mkdir -p /etc/gitpod-setup /workspace/bin /workspace/.ccache /workspace/.pyenv            \
   && sudo chown gitpod:gitpod /etc/gitpod-setup /workspace/bin /workspace/.ccache /workspace/.pyenv \
   && mkdir ~/.cache \
   && curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh -o /home/gitpod/brew-install.sh \
   && chmod +x /home/gitpod/brew-install.sh \
   && NONINTERACTIVE=1 /home/gitpod/brew-install.sh

# n.b.: Can't actually put anything persistent under `/workspace` in here because in Gitpod `/workspace`
# is an _external mountpoint_ and thus will hide anything there in this container.  But making sure env
# vars exist is fine, even putting the directories in the path (where nonexistent directories are ignored)
# is ok.
ENV CCACHE_DIR="/workspace/.ccache"
ENV PYENV_ROOT="/workspace/.pyenv"
ENV PATH="/workspace/bin:/workspace/.pyenv:/workspace/.pyenv/shims:/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin/:${PATH}"
ENV MANPATH="${MANPATH}:/home/linuxbrew/.linuxbrew/share/man"
ENV INFOPATH="{$INFOPATH}:/home/linuxbrew/.linuxbrew/share/info"
ENV HOMEBREW_NO_AUTO_UPDATE=1

echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/gitpod/.profile
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

sudo apt remove -y cmake                           \
   && brew install cmake                           \
   && brew install --build-from-source ccache      \
   && brew install --build-from-source abseil      \
   && brew install --build-from-source grpc        \
   && brew install --build-from-source bear


# or maybe: brew install --build-from-source $(brew deps --include-build bear) bear
