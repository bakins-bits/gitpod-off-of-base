sudo DEBIAN_FRONTEND=noninteractive apt install -yq --no-install-recommends \
automake \
autotools-dev \
bsdmainutils \
build-essential \
gdb \
keyboard-configuration \
lcov \
libboost-all-dev \
libboost-dev \
libevent-dev \
libminiupnpc-dev \
libnatpmp-dev \
libqrencode-dev \
libqt5core5a \
libqt5dbus5 \
libqt5gui5 \
libsqlite3-dev \
libtool \
libunwind-dev \
libzmq3-dev \
pkg-config \
python3-zmq \
qttools5-dev \
qttools5-dev-tools \
qtwayland5 \
systemtap \
systemtap-sdt-dev
